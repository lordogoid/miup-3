#include <tr1/unordered_map>
#include <deque>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <climits>
#define FOR(i, n) for(int i = 0; i < n; i++)

using namespace std;
using namespace std::tr1;

struct Edge {
	int flow, cap;
	int rflow, rcap;
};

struct Vertex {
	unordered_map<int, Edge> out;
	vector<int> level;
	bool visited;
};

Vertex vertices[3000];

int size;

bool level(int s, int t) {
	vector<int> levels(size);
	levels[s] = 1;
	deque<int> order;
	order.push_back(s);
	while (!order.empty()) {
		int src = order.front();
		order.pop_front();
		vertices[src].level.clear();
		for (unordered_map<int, Edge>::iterator it = vertices[src].out.begin();
		     it != vertices[src].out.end(); it++) {
			int dest = it->first;
			if (it->second.rflow == it->second.rcap) {
				continue;
			}
			if (!levels[dest]) {
				levels[dest] = levels[src] + 1;
				order.push_back(dest);
			}
			if (levels[dest] == levels[src] + 1) {
				vertices[src].level.push_back(dest);
			}
		}
	}
	return !!levels[t];
}

int dfspath(int s, int t, int bottleneck, vector<int>& v) {
	if (vertices[s].visited) {
		return -1;
	}
	v.push_back(s);
	if (s == t) {
		return bottleneck;
	}
	vertices[s].visited = true;
	FOR(i, vertices[s].level.size()) {
		int dest = vertices[s].level[i];
		Edge& e = vertices[s].out[dest];
		if (e.rflow == e.rcap) {
			continue;
		}
		int val = dfspath(dest, t, min(bottleneck, e.rcap), v);
		if (val != -1) {
			vertices[s].visited = false;
			return val;
		}
	}
	vertices[s].visited = false;
	return -1;
	v.pop_back();
}

int blockingflow(int s, int t) {
	vector<int> v;
	int total = 0;
	int f;
	while ((f = dfspath(s, t, INT_MAX, v)) != -1) {
		total += f;
		FOR(i, v.size() - 1) {
			int a = v[i], b = v[i + 1];
			vertices[a].out[b].rcap -= f;
			vertices[b].out[a].rcap += f;
			if (!vertices[a].out[b].rcap) {
				vertices[a].out.erase(b);
			}
		}
		v.clear();
	}
	return total;
}

int mflow(int s, int t) {
	int flow = 0;
	while (level(s, t)) {
		flow += blockingflow(s, t);
	}
	return flow;
}

int main() {
	int v, e;
	scanf("%d %d", &v, &e);
	size = v;
	FOR(i, e) {
		int from, to;
		scanf("%d %d", &from, &to);
		Edge n = {0, 1, 0, 1};
		Edge rev = {0, 1, 0, 1};
		vertices[from].out[to] = n;
		vertices[to].out[from] = rev;
	}
	int Cut, s, t;
	scanf("%d %d %d", &Cut, &s, &t);
	if (mflow(s, t) > Cut) {
		puts("Yes");
	} else {
		puts("No");
	}
	return 0;
}