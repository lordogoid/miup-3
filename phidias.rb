class TileSplitter
	def initialize(*sizes)
		@sizes = sizes
		@cache = {}
		@sizes.each {|size| @cache[size] = 0}
		@min_width = @sizes.map(&:first).min
		@min_height = @sizes.map(&:last).min
	end
	
	def split(width, height)
		pair = [width, height]
		cur = @cache[pair]
		return cur if cur
		return @cache[pair] = width * height if width < @min_width or height < @min_height
		cur = Float::INFINITY
		1.upto(width / 2) do |n|
			val = split(n, height) + split(width - n, height)
			cur = val if val < cur
		end
		1.upto(height / 2) do |n|
			val = split(width, n) + split(width, height - n)
			cur = val if val < cur
		end
		@cache[pair] = cur
	end
end

n_cases = gets.to_i
n_cases.times do
	width, height = gets.split.map &:to_i
	n_sizes = gets.to_i
	sizes = n_sizes.times.map { gets.split.map &:to_i }
	puts TileSplitter.new(*sizes).split(width, height)
end