#include <stdio.h>
#define FOR(i, n) for(int i = 0; i < n; i++)

int pos[256];

int main() {
	pos['+'] = 0;
	pos['-'] = 1;
	pos['>'] = 2;
	pos['<'] = 3;
	pos['.'] = 4;
	pos[','] = 5;
	pos['['] = 6;
	pos[']'] = 7;
	int c, p = -1;
	while ((c = getchar()) != EOF) {
		if (!pos[c] && c != '+') {
			continue;
		}
		int d = pos[c] - p;
		p = pos[c];
		if (d <= 0) {
			d += 8;
		}
		d--;
		while (d--) {
			putchar('_');
		}
		putchar('!');
	}
	putchar('\n');
	return 0;
}

