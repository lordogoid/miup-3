#include <string.h>
#include <ctype.h>
#include <stdio.h>

char readbuf[16384], writebuf[16384];

void greatest_div15(const char* src, char* dest) {
	int digits[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int dsum = 0;
	int ends_with_5 = 0;
	for (const char* p = src; isdigit(*p); p++) {
		int digit = *p - '0';
		digits[digit]++;
		dsum += digit;
	}
	// Check divisibility by 5
	if (!digits[5] && !digits[0]) {
		goto zero;
	} else if (!digits[0]) {
		ends_with_5 = 1;
		digits[5]--;
	}
	// Check divisibility by 3
	switch (dsum % 3) {
		case 1:
		if (digits[1]) {
			digits[1]--;
			dsum--;
		} else if (digits[4]) {
			digits[4]--;
			dsum -= 4;
		} else if (digits[7]) {
			digits[7]--;
			dsum -= 7;
		} else if (digits[2] >= 2) {
			digits[2] -= 2;
			dsum -= 4;
		} else if (digits[5] >= 2) {
			digits[5] -= 2;
			dsum -= 10;
		} else if (digits[8] >= 2) {
			digits[8] -= 2;
			dsum -= 16;
		} else {
			goto zero;
		}
		break;
		case 2:
		if (digits[2]) {
			digits[2]--;
			dsum -= 2;
		} else if (digits[5]) {
			digits[5]--;
			dsum -= 5;
		} else if (digits[8]) {
			digits[8]--;
			dsum -= 8;
		} else if (digits[1] >= 2) {
			digits[1] -= 2;
			dsum -= 2;
		} else if (digits[4] >= 2) {
			digits[4] -= 2;
			dsum -= 8;
		} else if (digits[7] >= 2) {
			digits[7] -= 2;
			dsum -= 14;
		} else {
			goto zero;
		}
	}
	if (dsum == 0) {
		goto zero;
	}
	int pos = 0;
	for (int i = 9; i >= 0; i--) {
		for (int j = 0; j < digits[i]; j++) {
			dest[pos++] = i + '0';
		}
	}
	if (ends_with_5) {
		dest[pos++] = '5';
	}
	dest[pos] = 0;
	return;
zero:
	strcpy(dest, digits[0] ? "0" : "impossible");
}

int main() {
	int n;
	scanf("%d\n", &n);
	while (n--) {
		fgets(readbuf, sizeof(readbuf), stdin);
		greatest_div15(readbuf, writebuf);
		printf("%s\n", writebuf);
	}
	return 0;
}