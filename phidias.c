#include <stdio.h>
#include <string.h>
#include <limits.h>

static int slab_width;
static int wasted[601][601];
static int min_width = INT_MAX, min_height = INT_MAX;

static int calc_wasted(int width, int height) {
	if (wasted[width][height] != -1) return wasted[width][height];
	if (width < min_width || height < min_height) {
		wasted[width][height] = width * height;
		return wasted[width][height];
	}
	int best = INT_MAX;
	for (int j = 1; j <= width / 2; j++) {
		int next = calc_wasted(j, height) + calc_wasted(width - j, height);
		if (next < best) best = next;
		if (best == 0) goto rock_bottom;
	}
	for (int j = 1; j <= height / 2; j++) {
		int next = calc_wasted(width, j) + calc_wasted(width, height - j);
		if (next < best) best = next;
		if (best == 0) goto rock_bottom;
	}
rock_bottom:
	wasted[width][height] = best;
	return best;
}

static int calc_wasted2(int width, int height) {
	/*for (int i = 1; i < min_width; i++) {
		for (int j = 1; j <= height; j++) {
			wasted[i][j] = i * j;
		}
	}
	for (int i = 1; i <= width; i++) {
		for (int j = 1; j < min_height; j++) {
			wasted[i][j] = i * j;
		}
	}*/
	for (int i = 1; i <= width; i++) {
		for (int j = 1; j <= height; j++) {
			if (wasted[i][j] == 0) continue;
			int best = i * j;
			for (int s = 1; s <= i / 2; s++) {
				int next = wasted[s][j] + wasted[i - s][j];
				if (next < best) best = next;
			}
			for (int s = 1; s <= j / 2; s++) {
				int next = wasted[i][s] + wasted[i][j - s];
				if (next < best) best = next;
			}
			wasted[i][j] = best;
		}
	}
	return wasted[width][height];
}

int main() {
	int n_cases;
	scanf("%d", &n_cases);
	for (int i = 0; i < n_cases; i++) {
		memset(wasted, -1, sizeof(wasted));
		int height, n_plates;
		scanf("%d %d %d", &slab_width, &height, &n_plates);
		for (int j = 0; j < n_plates; j++) {
			int pwidth, pheight;
			scanf("%d %d", &pwidth, &pheight);
			wasted[pwidth][pheight] = 0;
			if (pwidth < min_width) min_width = pwidth;
			if (pheight < min_height) min_height = pheight;
		}
		printf("%d\n", calc_wasted2(slab_width, height));
	}
	return 0;
}