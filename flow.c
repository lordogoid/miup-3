#include <stdio.h>
#include <stdlib.h>

#define FOR(i, n) for(int i = 0; i < n; i++)

signed char flow[2500][2500];
signed char cap[2500][2500];

char visited[2500];

int size;

inline int rcap(int s, int t) {
	return cap[s][t] - flow[s][t];
}

int dfs(int s, int t, int bottleneck) {
	if (visited[s]) {
		return 0;
	}
	if (s == t) {
		return bottleneck;
	}
	visited[s] = 1;
	FOR(i, size) {
		int rc = rcap(s, i);
		if (rc > 0) {
			int b = dfs(i, t, min(bottleneck, rc));
			if (b) {
				flow[s][i] += b;
				flow[i][s] -= b;
				visited[s] = 0;
				return b;
			}
		}
	}
	visited[s] = 0;
	return 0;
}

int mflow(int s, int t) {
	FOR(i, size) {}
}

int main() {
	int v, e;
	scanf("%d %d", &v, &e);
	size = v;
	FOR(i, e) {
		int from, to;
		scanf("%d %d", &from, &to);
		cap[from][to] = 1;
		cap[to][from] = 1;
	}
	int Cut, s, t;
	scanf("%d %d %d", &Cut, &s, &t);
	if (mflow(s, t) > Cut) {
		puts("Yes");
	} else {
		puts("No");
	}
	return 0;
}